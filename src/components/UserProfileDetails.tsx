import React, { FC, memo } from 'react';
import { motion } from 'framer-motion';
import { Row, Col, Divider, Typography, Space } from 'antd';
import { useSelector } from 'react-redux';
import { selectDetails } from '../store/selectors/selectors';
import {
   imageVariants,
   biovariants,
   detailsVariants,
} from '../styles/animations.details';

const { Text, Title } = Typography;

const UserProfileDetails: FC = memo(() => {
   const details = useSelector(selectDetails);
   return (
      <Row gutter={[20, 20]}>
         <Col md={11} span={24}>
            <motion.div
               className="single"
               initial="exit"
               animate="enter"
               exit="exit"
            >
               <motion.img
                  className="avatar_image"
                  variants={imageVariants}
                  src={
                     details.items?.avatar_url ??
                     'http://s3.amazonaws.com/37assets/svn/765-default-avatar.png'
                  }
                  alt="The Barbican"
               />
            </motion.div>
         </Col>

         <Col md={13} span={24}>
            <motion.div
               className="single"
               initial="exit"
               animate="enter"
               exit="exit"
            >
               <motion.div variants={detailsVariants}>
                  <Space direction="horizontal">
                     <Text type="secondary" strong={true}>
                        UserName:{' '}
                     </Text>
                     <Title level={5}>{details.items?.name ?? '-'}</Title>
                  </Space>
                  <Divider className="details_divider" />
                  <Space direction="horizontal">
                     <Text type="secondary" strong={true}>
                        Email:{' '}
                     </Text>
                     <Title level={5}>{details.items?.email ?? '-'}</Title>
                  </Space>
                  <Divider className="details_divider" />
                  <Space direction="horizontal">
                     <Text type="secondary" strong={true}>
                        Location:{' '}
                     </Text>
                     <Title level={5}>{details.items?.location ?? '-'}</Title>
                  </Space>
                  <Divider className="details_divider" />
                  <Space direction="horizontal">
                     <Text type="secondary" strong={true}>
                        Join Date:{' '}
                     </Text>
                     <Title level={5}>
                        {details.items?.created_at &&
                           new Date(
                              details.items.created_at,
                           ).toLocaleDateString()}
                     </Title>
                  </Space>
                  <Divider className="details_divider" />
                  <Space direction="horizontal">
                     <Text type="secondary" strong={true}>
                        Followers:{' '}
                     </Text>
                     <Title level={5}>{details.items?.followers ?? 0}</Title>
                  </Space>
                  <Divider className="details_divider" />
                  <Space direction="horizontal">
                     <Text type="secondary" strong={true}>
                        Followers:{' '}
                     </Text>
                     <Title level={5}>{details.items?.followers ?? 0}</Title>
                  </Space>
               </motion.div>
            </motion.div>
         </Col>
         <Col span={24}>
            <Divider className="details_divider" />
            <motion.div initial="exit" animate="enter" exit="exit">
               <motion.div variants={biovariants}>
                  <Space direction="vertical">
                     <Text type="secondary" strong={true}>
                        Biography:
                     </Text>
                     <Title level={5}>{details.items?.bio ?? '-'}</Title>
                  </Space>
               </motion.div>
            </motion.div>
         </Col>
         <Divider />
      </Row>
   );
});

export default UserProfileDetails;
