import React, { FC } from 'react';
import { Divider, Typography } from 'antd';

const { Title } = Typography;

const Header: FC<{}> = () => {
   return (
      <header className="header">
         <Title level={2}>Github Searcher</Title>
         <Divider />
      </header>
   );
};

export default Header;
