import React, { FC } from 'react';
import { List, Space } from 'antd';
import { IReposList } from '../types/components';
import Title from 'antd/es/typography/Title';
import { motion } from 'framer-motion';

const ReposLIst: FC<IReposList> = ({ repositories }) => {
   console.log('', repositories);
   return (
      <List
         itemLayout="horizontal"
         dataSource={repositories}
         // loading={loading}
         renderItem={({ name, forks, stargazers_count, html_url }) => (
            <motion.div whileHover={{ scale: 1.05 }} whileTap={{ scale: 0.95 }}>
               <a href={html_url} target="_blank">
                  <List.Item>
                     <List.Item.Meta avatar={<Title level={4}>{name}</Title>} />
                     <Space direction={'vertical'}>
                        <Title level={5}>Forks: # {forks}</Title>
                        <Title level={5}>Stars: # {stargazers_count}</Title>
                     </Space>
                  </List.Item>
               </a>
            </motion.div>
         )}
      />
   );
};

export default ReposLIst;
