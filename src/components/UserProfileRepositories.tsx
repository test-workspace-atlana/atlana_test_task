import React, { FC, memo, useCallback } from 'react';
import { IUserRepos } from '../types/components';
import UsersSearchInput from './UsersSearchInput';
import ReposLIst from './ReposLIst';
import { useDispatch, useSelector } from 'react-redux';
import { searchUsersRepos } from '../store/actions/actions';
import { selectReposAndFilter } from '../store/selectors/selectors';
import { motion } from 'framer-motion';
import { biovariants, transition } from '../styles/animations.details';

const UserProfileRepositories: FC = memo(() => {
   const { value, repositories } = useSelector(selectReposAndFilter);
   const dispatch = useDispatch();
   const handleSearch = useCallback(
      ({ target: { value } }) => {
         dispatch(searchUsersRepos(value));
      },
      [dispatch],
   );
   return (
      <motion.div initial="exit" animate="enter" exit="exit">
         <motion.div variants={biovariants}>
            <UsersSearchInput
               size="large"
               placeholder="Enter repo name"
               value={value}
               onChange={handleSearch}
            />
            <ReposLIst repositories={repositories} />
         </motion.div>
      </motion.div>
   );
});

export default UserProfileRepositories;
