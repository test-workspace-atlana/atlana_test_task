import React from 'react';
import { LoadingOutlined } from '@ant-design/icons';
import { Spin } from 'antd';

const Preloader = () => {
   return (
      <div className="preloader">
         <Spin indicator={<LoadingOutlined style={{ fontSize: 36 }} spin />} />
      </div>
   );
};

export default Preloader;
