import React, { useState, useEffect, FC } from 'react';
import { Input } from 'antd';
import { Divider } from 'antd';
import { IUsersSearch } from '../types/components';

const UsersSearchInput: FC<IUsersSearch> = ({ value, onChange, ...rest }) => {
   return (
      <>
         <Input value={value} onChange={onChange} {...rest} />
         <Divider />
      </>
   );
};

export default UsersSearchInput;
