import React, { FC } from 'react';
import { IUsersList } from '../types/components';
import { Spin , Typography, Empty } from 'antd';
import { LoadingOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { isNull, isEmpty } from 'lodash';
import { motion } from 'framer-motion';
import {
   imageVariants,
   frameVariants,
   thumbnailVariants,
   transition,
} from '../styles/animations.search';
import Preloader from "./Preloader";

const { Title } = Typography;

const UsersList: FC<IUsersList> = ({ users, loading }) => {
   console.log('users', users);
   if (isNull(users)) {
      return <Title level={4}>Welcome!</Title>;
   }
   if (isEmpty(users)) {
      return <Empty />;
   }
   if (loading) {
      return <Preloader/>
   }
   return (
      <motion.div
         className="search-list"
         initial="initial"
         animate="enter"
         exit="exit"
         variants={{ exit: { transition: { staggerChildren: 0.1 } } }}
      >
         {users.map(({ avatar_url, login, public_repos: { count } }, i) => (
            <motion.div key={login} variants={thumbnailVariants}>
               <motion.div
                  whileHover="hover"
                  variants={frameVariants}
                  transition={transition}
               >
                  <Link to={`/${login}`} className="search-list-item">
                     <motion.img
                        src={avatar_url}
                        alt="The Barbican"
                        variants={imageVariants}
                        transition={transition}
                     />
                     <Title level={5}>{login}</Title>
                     <Title className="last-title-item" level={5}>
                        Repos: # {count}
                     </Title>
                  </Link>
               </motion.div>
            </motion.div>
         ))}
      </motion.div>
   );
};

export default UsersList;
