import React, { FC, useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { githubUsersSelector } from '../store/selectors/selectors';
import { searchUsersAction } from '../store/actions/actions';
import UsersSearchInput from '../components/UsersSearchInput';
import UsersList from '../components/UsersList';
import { SGithubUsers } from '../types/store';
import { motion } from 'framer-motion';
import {easetransition, easeimageVariants} from "../styles/animations.search";


const UsersSearch: FC<{}> = () => {
   const { search_text, github_users, loading }: SGithubUsers = useSelector(
      githubUsersSelector,
   );
   const dispatch = useDispatch();
   const handleSearch = useCallback(
      ({ target: { value } }) => {
         dispatch(searchUsersAction(value));
      },
      [dispatch],
   );
   return (
      <div>
         <motion.div
            className="single"
            initial="exit"
            animate="enter"
            exit="exit"
            variants={easeimageVariants}
         >
            <UsersSearchInput
               size="large"
               placeholder="Enter username for search with github"
               value={search_text}
               onChange={handleSearch}
            />
         </motion.div>
         <UsersList users={github_users} loading={loading} />
      </div>
   );
};

export default UsersSearch;
