import React, { useState, useEffect, FC } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useParams } from 'react-router-dom';
import { getUserProfile } from '../store/actions/actions';
import { selectUserProfile } from '../store/selectors/selectors';
import UserProfileDetails from '../components/UserProfileDetails';
import UserProfileRepositories from '../components/UserProfileRepositories';

const UserProfile: FC<{}> = () => {
   let { id } = useParams<{ id: string }>();
   const dispatch = useDispatch();
   useEffect(() => {
      dispatch(getUserProfile(id));
   }, [id, dispatch]);

   return (
      <div className="profile">
         <UserProfileDetails />
         <UserProfileRepositories />
      </div>
   );
};

export default UserProfile;
