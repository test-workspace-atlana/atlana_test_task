import axios, { AxiosError, AxiosInstance, AxiosResponse } from 'axios';
import { URLS } from '../types/enums';

// abstract class ApiService {
//    private instance: InstanceType<any>;
//    protected constructor(public inst: Object) {}
//    fetchAllUsers: (query: string) => Promise<ResponseType>;
// }
class Api {
   private instance: AxiosInstance;
   constructor() {
      this.instance = axios.create({
         baseURL: URLS.BASE_URL,
         headers: {
            'Content-Type': 'application/json',
            Authorization: 'token 499beb795ef33abf11524ea578cbc43e38ee2617',
         },
      });
   }
   fetchAllUsers = (query: string): Promise<AxiosResponse> =>
      this.instance.get('/search/users', {
         params: {
            q: `${query} in:login`,
            per_page: 9,
            page: 1,
         },
      });
   fetchUserDetails = (name: string): Promise<AxiosResponse> =>
      this.instance.get(`/users/${name}`);
   fetchUserRepositoriesProfileName = (name: string): Promise<AxiosResponse> =>
      this.instance.get(`/users/${name}/repos`);

   fetchUserRepositoriesByName = (name: string): Promise<AxiosResponse> =>
      // @ts-ignore
      this.instance
         .get(`/search/repositories?q=user:${name}`)
         .catch((e) => ({ config: e.response.config, data: e.response.data }));
}
const api = new Api();

export default api;
