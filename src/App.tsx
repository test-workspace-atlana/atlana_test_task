import React from 'react';
import { Route, Switch, Redirect } from 'react-router-dom';
import UserProfile from './containers/profile';
import UserSearch from './containers/search';
import { Col, Row } from 'antd';
import Header from './components/Header/Header';
import { AnimatePresence } from 'framer-motion';
import NotFound from './containers/404';

function App() {
   return (
      <div className="App">
         <Row>
            <Col span={14} offset={5}>
               <Header />
               <Route
                  render={({ location }) => (
                     <AnimatePresence exitBeforeEnter initial={true}>
                        <Switch>
                           <Route exact path="/" component={UserSearch} />
                           <Route exact path="/:id" component={UserProfile} />
                           <Route component={NotFound} />
                        </Switch>
                     </AnimatePresence>
                  )}
               />
            </Col>
         </Row>
      </div>
   );
}

export default App;
