import { ChangeEvent } from 'react';
import {IPersonalData, ISearchUser, IUserRepositories, UserReposInfo} from './store';
import { InputProps } from 'antd';

export interface IUsersSearch extends InputProps {
   onChange(d: ChangeEvent<HTMLInputElement>): void;
   value: string;
   children?: JSX.Element;
}
export interface IUsersList {
   users: ISearchUser[] | null;
   children?: JSX.Element;
   loading: boolean;
}
export interface IDetails {
   details: IPersonalData;
   children?: JSX.Element;
}
export interface IUserRepos {
   value: string;
   repositories: IUserRepositories;
   children?: JSX.Element;
}
export interface IReposList {
   repositories: UserReposInfo[];
   loading?: boolean;
}
