import { ACTION_TYPES, ERROR_TYPES } from './enums';
import { AxiosError } from 'axios';

export interface IStore {
   github_users: SGithubUsers;
   profile_details: SUserProfile;
}
export interface SUserProfile {
   search_text: string;
   details: IPersonalData;
   repositories: IUserRepositories;
   error: AxiosError | null;
}

export interface PersonalUserInfo {
   avatar_url: string;
   name: string;
   email: string | null;
   location: string | null;
   created_at: Date;
   followers: number;
   following: number;
   bio: string | null;
}

export interface UserReposInfo {
   name: string;
   forks: number;
   stargazers_count: number;
   html_url: string
}
export interface IPersonalData {
   loading: boolean;
   items: PersonalUserInfo | null;
}
export interface IUserRepositories {
   loading: boolean;
   items: UserReposInfo[];
}
//TODO should create enum for statuses and message type but have no time

export type IReposResponse = {
   count: number;
   items: any[];
};
export type ISearchUser = {
   login: string;
   id: number;
   avatar_url: string;
   public_repos: IReposResponse;
};

export interface SGithubUsers {
   search_text: string;
   github_users: ISearchUser[] | null;
   loading: boolean;
   error: AxiosError | null;
}

type ActionPayloadType =
   | ISearchUser[]
   | AxiosError
   | PersonalUserInfo
   | UserReposInfo[]
   | string;

export interface ActionType {
   type: ACTION_TYPES | ERROR_TYPES;
   payload?: ActionPayloadType;
}
