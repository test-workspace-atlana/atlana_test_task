import { takeEvery, call, all, put } from 'redux-saga/effects';
import { ACTION_TYPES } from '../../types/enums';
import {
   fetchUserRepositoriesSuccess,
   fetchUserDetailsSuccess, fetchProfileError,
} from '../actions/actions';
import api from '../../services/api';

function* fetchUserRepositories(payload: any) {
   const { data } = yield call(api.fetchUserRepositoriesProfileName, payload);
   yield put(fetchUserRepositoriesSuccess(data));
}
function* fetchUserDetails(payload: any) {
   const { data } = yield call(api.fetchUserDetails, payload);
   yield put(fetchUserDetailsSuccess(data));
}
function* fetchProfileSummary({ payload }: any) {
   try {
      yield all([
         call(fetchUserDetails, payload),
         call(fetchUserRepositories, payload),
      ]);
   } catch (error) {
      yield put(fetchProfileError(error));
   }
}

export function* watchUserProfile() {
   yield takeEvery(ACTION_TYPES.RETRIEVE_USER_PROFILE, fetchProfileSummary);
}
