import { all, takeLatest } from 'redux-saga/effects';
import { watchSearchInput } from './users';
import { ERROR_TYPES } from '../../types/enums';
import { SagasErrorHandler } from './errorHandler';
import { watchUserProfile } from './profile';

function* watchError() {
   yield takeLatest(
      [ERROR_TYPES.FETCH_ALL_USERS_ERROR, ERROR_TYPES.FETCH_USER_PROFILE_ERROR],
      SagasErrorHandler,
   );
}
export default function* rootSaga() {
   yield all([watchSearchInput(), watchError(), watchUserProfile()]);
}
