import {
   put,
   call,
   debounce,
   all,
   take,
   fork,
   cancel,
} from 'redux-saga/effects';
import { ACTION_TYPES } from '../../types/enums';
import { ISearchUser, IReposResponse } from '../../types/store';
import api from '../../services/api';
import { isEmpty } from 'lodash';
import {
   fetchUsersRequest,
   fetchUsersError,
   fetchUsersSuccess,
} from '../actions/actions';
import { mapUsersToCounts } from '../../helpers/helpers';
import { AxiosResponse } from 'axios';

const DEBOUNCE_DELAY = 600;

function* fetchAllUsers({ payload }: any) {
   try {
      yield put(fetchUsersRequest());
      const {
         data: { items },
      }: AxiosResponse<{ items: ISearchUser[] }> = yield call(
         api.fetchAllUsers,
         payload,
      );

      const allUsersRepos: AxiosResponse<IReposResponse>[] = yield all(
         items.map(({ login }) => call(api.fetchUserRepositoriesByName, login)),
      );

      const mapRepos = mapUsersToCounts(allUsersRepos);
      const mergeReposInUsers: ISearchUser[] = items.map((user) => ({
         ...user,
         public_repos: mapRepos[user.login],
      }));

      yield put(fetchUsersSuccess(mergeReposInUsers));
   } catch (error: any) {
         yield put(fetchUsersError(error));

   }
}
function* watchInputAndDebounceFetch() {
   yield debounce(
      DEBOUNCE_DELAY,
      ACTION_TYPES.USERS_SEARCH_CHANGE,
      fetchAllUsers,
   );
}
export function* watchSearchInput() {
   let task = yield fork(watchInputAndDebounceFetch);
   while (true) {
      const { payload } = yield take(ACTION_TYPES.USERS_SEARCH_CHANGE);
      //cancel saga if input is empty

      if (isEmpty(payload)) {
         yield cancel(task);
      }
   }
}
