import { notification } from 'antd';

export function* SagasErrorHandler({ payload }: any) {
   // console.log('payload.response', payload.response);
   if (payload.response) {
      notification.error({
         message: payload.response?.status,
         description: payload.response?.data?.message,
      });
   } else {
      notification.error({
         message: 500,
         description: 'Server_Error. Try again later.',
      });
   }
}
