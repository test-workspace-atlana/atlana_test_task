import { SGithubUsers, ActionType } from '../../types/store';
import { ACTION_TYPES, ERROR_TYPES } from '../../types/enums';

const initial_state: SGithubUsers = {
   search_text: '',
   github_users: null,
   loading: false,
   error: null,
};

export default function github_users(
   state = initial_state,
   { type, payload }: ActionType,
): SGithubUsers {
   switch (type) {
      case ACTION_TYPES.USERS_SEARCH_CHANGE:
         return { ...state, search_text: payload, error: null } as SGithubUsers;
      case ACTION_TYPES.FETCH_ALL_USERS:
         return { ...state, error: null, loading: true };
      case ACTION_TYPES.FETCH_ALL_USERS_SUCCESS:
         return {
            ...state,
            error: null,
            loading: false,
            github_users: payload,
         } as SGithubUsers;
      case ERROR_TYPES.FETCH_ALL_USERS_ERROR:
         return { ...state, error: payload, loading: false } as SGithubUsers;
      default:
         return state;
   }
}
