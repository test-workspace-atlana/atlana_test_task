import { SUserProfile, ActionType } from '../../types/store';
import { ACTION_TYPES, ERROR_TYPES } from '../../types/enums';

const initial_state: SUserProfile = {
   search_text: '',
   details: {
      loading: false,
      items: null,
   },
   repositories: {
      loading: false,
      items: [],
   },
   error: null,
};

export default function profile_details(
   state = initial_state,
   { type, payload }: ActionType,
): SUserProfile {
   switch (type) {
      case ACTION_TYPES.FETCH_USER_PROFILE:
         return {
            ...state,
            error: null,
            details: { ...state.details, loading: true },
         } as SUserProfile;
      case ACTION_TYPES.FETCH_USER_PROFILE_SUCCESS:
         return {
            ...state,
            details: {
               loading: false,
               items: payload,
            },
            error: null,
         } as SUserProfile;
      case ACTION_TYPES.FETCH_USER_REPOS:
         return {
            ...state,
            repositories: { ...state.repositories, loading: true },
            error: null,
         } as SUserProfile;
      case ACTION_TYPES.FETCH_USER_REPOS_SUCCESS:
         return {
            ...state,
            repositories: {
               loading: false,
               items: payload,
            },
            error: null,
         } as SUserProfile;
      case ACTION_TYPES.USER_SEARCH_REPOS:
         return {
            ...state,
            search_text: payload,
            error: null,
         } as SUserProfile;

      case ERROR_TYPES.FETCH_USER_PROFILE_ERROR:
         return { ...state, error: payload } as SUserProfile;
      default:
         return state;
   }
}
