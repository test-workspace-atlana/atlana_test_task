import { combineReducers } from 'redux';
import { IStore } from '../../types/store';
import github_users from './search_user';
import profile_details from './profile_details';

const reducers = combineReducers<IStore>({
   github_users,
   profile_details,
});
export default reducers;
