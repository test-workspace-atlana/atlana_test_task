import {
   SGithubUsers,
   IStore,
   SUserProfile,
   UserReposInfo,
} from '../../types/store';
import { createSelector } from 'reselect';

export const githubUsersSelector = (state: IStore): SGithubUsers =>
   state.github_users;

const selectInputChange = (state: IStore) => state.profile_details.search_text;

export const selectUserProfile = (state: IStore): any =>
   state.profile_details.repositories.items;
export const selectDetails = (state: IStore) => state.profile_details.details;
export const selectReposAndFilter = createSelector(
   selectInputChange,
   selectUserProfile,
   (input, repos: UserReposInfo[]) => {
      return {
         value: input,
         repositories: repos.filter((item) => item.name.includes(input)),
      };
   },
);
