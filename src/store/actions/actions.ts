import { ACTION_TYPES, ERROR_TYPES } from '../../types/enums';
import {
   ActionType,
   ISearchUser,
   PersonalUserInfo,
   UserReposInfo,
} from '../../types/store';
import { AxiosError } from 'axios';

export const searchUsersAction = (value: string): ActionType => ({
   type: ACTION_TYPES.USERS_SEARCH_CHANGE,
   payload: value,
});
export const searchUsersRepos = (value: string): ActionType => ({
   type: ACTION_TYPES.USER_SEARCH_REPOS,
   payload: value,
});

export const fetchUsersRequest = (): ActionType => ({
   type: ACTION_TYPES.FETCH_ALL_USERS,
});
export const fetchUsersSuccess = (users: ISearchUser[]): ActionType => ({
   type: ACTION_TYPES.FETCH_ALL_USERS_SUCCESS,
   payload: users,
});
export const fetchUsersError = (error: AxiosError): ActionType => ({
   type: ERROR_TYPES.FETCH_ALL_USERS_ERROR,
   payload: error,
});
export const fetchProfileError = (error: AxiosError): ActionType => ({
   type: ERROR_TYPES.FETCH_USER_PROFILE_ERROR,
   payload: error,
});
//
export const getUserProfile = (id: string): ActionType => ({
   type: ACTION_TYPES.RETRIEVE_USER_PROFILE,
   payload: id,
});

export const fetchUserDetailsSuccess = (data: UserReposInfo[]): ActionType => ({
   type: ACTION_TYPES.FETCH_USER_PROFILE_SUCCESS,
   payload: data,
});
export const fetchUserRepositoriesSuccess = (
   data: PersonalUserInfo,
): ActionType => ({
   type: ACTION_TYPES.FETCH_USER_REPOS_SUCCESS,
   payload: data,
});
