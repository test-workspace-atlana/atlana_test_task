
export const transition = { duration: 0.5, ease: [0.43, 0.13, 0.23, 0.96] };

export const thumbnailVariants = {
    initial: { y: 25, opacity: 0 },
    enter: { y: 0, opacity: 1, transition },
    exit: {
        y: 25,
        opacity: 0,
        // @ts-ignore
        transition: { duration: 1.5, ...transition },
    },
};

export const frameVariants = {
    hover: { scale: 0.95 },
};

export const imageVariants = {
    hover: { scale: 1.1 },
};

export const easetransition = {
    duration: 1,
    ease: [0.43, 0.13, 0.23, 0.96],
};
export const easeimageVariants = {
    exit: { y: '-25%', opacity: 0, easetransition },
    enter: {
        y: '0%',
        opacity: 1,
        easetransition,
    },
};
