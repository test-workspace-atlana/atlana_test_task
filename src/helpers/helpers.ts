import { IReposResponse } from '../types/store';
import { AxiosRequestConfig, AxiosResponse } from 'axios';

const getUserNameFromUrl = (url: string): string =>
    // @ts-ignore
    url.match(/(?<=user:).+/g)[0] ?? '';

export const mapUsersToCounts = (
    repos: AxiosResponse<IReposResponse>[],
): Record<string, IReposResponse> =>
    repos.reduce(
        // @ts-ignore
        (resultMap, { config, data }: any) => console.log(data) ||({
            ...resultMap,
            [getUserNameFromUrl(config.url)]: {
                count: data.errors ? 0 : data.total_count,
                items: data.errors ? [] : data.items,
            },
        }),
        {},
    );
